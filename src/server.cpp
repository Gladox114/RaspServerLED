#include<iostream>
#include<string>
#include<vector>
#include<fstream>
#include<filesystem>

// this executes a a.out file
void run_exec(std::string path) { 
    std::string command = "cd " + path + " && ./a.out";
    std::cout << command << std::endl;
    system(command.c_str());
};

//this will execute a command
class executor { 
public:
    executor(std::string path);
    ~executor();
    static int counter;
};

int executor::counter = 0;

executor::executor(std::string path) {
    std::cout << "executor constructor" << std::endl;
    counter++;
    run_exec(path);
}

executor::~executor() {
    std::cout << "executor destructor" << std::endl;
    counter--;
}

using std::filesystem::directory_iterator;
// this lists all files in a directory
void list_files(std::string path) { 
    for (const auto & file : directory_iterator(path))
        std::cout << file.path() << std::endl;
}

int main(){
    // This is a server that will accept commands from a client and execute them.
    // executor e{"C:\\Users\\joe\\Desktop\\test.exe"};
    // list_files("/home/roman/Desktop/share/RaspServerLED/");
    // list_files("./server.cpp");
    run_exec("../programs/test.py");

    
    
    return 0;
}
